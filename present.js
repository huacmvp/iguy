var fs = require('fs');
var marked = require('marked');
var TerminalRenderer = require('marked-terminal');

marked.setOptions({renderer: new TerminalRenderer({})});

var input = fs.readFileSync('init/slides.md', 'utf8');

console.log(marked(input));


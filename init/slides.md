# Init

![Git logo](rscs/img/acm.jpg)
![ACM logo](rscs/img/git.png)
Getting started with Git

---

### I'm an egotistical bastard, and I name all my projects after
### myself. First Linux, now git

---

# Describe

### "Distributed revision control and source code management system" - Wikipedia

- Created in 2005 by Linux Trovalds
- Now the most popular SCM in the world
- Gives each developer a local copy of the source code
- Cross Platform
- Focus on feature based workflow & frictionless switching with branching

---

# Config

DAG >> States >> Blobs >> Commits >> Branches >>
Remotes >> Tags >> Trees Oh My!

- [Git Basics](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)
- [Git is Simpler Than You Think](https://nfarina.com/post/98685/git-is-simpler)
- [Git for Computer Scientist](http://eagain.net/articles/git-for-computer-scientists/)

---

# Man Linux

## Gnu commands we'll cover (more coming soon!):
- cd: change current working folder
- ls: list what's in the current folder
- rm: remove
- mv: move
- cp: copy
- ssh-keygen: generate a key pair

---

# Clone
### Command examples

---

# Man Git

## Git commands we'll cover:
- init: creates a git repo
- add: moves files to staging area
- commit:  commit create a copy to send to remote
- pull: fetches the latest copy from remote
- push: sends latest copy to remote
- clone: copies a remote repo to your machine

---

# Clone
### Command examples

---

# Fetch(Demo)
### Assignment at https://bitbucket.org/huacmvp/init
1. Clone the repo
2. Add your name to the list (watch out!)
3. Push your changes back to the remote

---

# Remote
### Where does all my stuff go?!
[Octocat](rscs/img/octocat.png)
[Bucket](rscs/img/bucket.png)
[Github logo](rscs/img/github.png)
[Bitbucket logo](rscs/img/bit.png)

---

# Sources
- wiki
- wiki
- atlassian
- github
- blog.dbrgn.ch
- speaderdeck.com

---
